CREATE DATABASE IF NOT EXISTS `site` COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `site`.`page` (
    `id` int NOT NULL AUTO_INCREMENT,
    `title` tinytext NOT NULL,
    `content` text NOT NULL,
    PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;