<?php

class Router 
{
    private $url;
    private $routes;

    public function __construct() {
        $this->url = $_SERVER['REQUEST_URI'];
        $this->routes = [];
    }

    public function addRoute($regex, $handler) {
        $this->routes[] = [
           'regex' => $regex,
           'handler' => $handler 
        ];
    }

    public function route() {
        foreach ($this->routes as $entry) {
            $regex = $entry['regex'];
            $handler = $entry['handler'];
            if (preg_match($regex, $this->url, $matches)) {
                $handler(...array_slice($matches, 1));
                break;
            }
        }
    }
}