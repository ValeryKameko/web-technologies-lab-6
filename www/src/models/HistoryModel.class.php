<?php

namespace Model;

class HistoryModel {
    public function __construct() {
        \session_start();
    }

    public function getEntries() {
        return $_SESSION['history_events'];
    }

    public function clearEntries() {
        $_SESSION['history_events'] = [];
    }

    public function addEntry($entry) {
        array_push($_SESSION['history_events'], $entry);
    }

    public function addEntries(...$entries) {
        array_push($_SESSION['history_events'], ...$entries);
    }

    public function initialize() {
        if (!$this->isInitialized()) {
            $_SESSION['history_events'] = [];
            $_SESSION['inialized'] = true;
        }
    }

    public function uninitialize() {
        unset($_SESSION);
    }

    public function isInitialized() {
        return isset($_SESSION['inialized']) && $_SESSION['inialized'] === true;
    }
}