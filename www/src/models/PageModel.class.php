<?php

namespace Model;

class PageModel {
    public function __construct($db) {
        $this->db = $db;
    }

    public function getPage($id) {
        $sql = 'SELECT `page`.*
                FROM `page` 
                WHERE `page`.`id` = :id';
        $query = $this->db->prepare($sql);
        $query->execute([
            ':id' => $id
        ]);
        return $query->fetch(\PDO::FETCH_ASSOC);
    }
    
    public function getPageIds() {
        $sql = 'SELECT `page`.`id`
                FROM `page`';
        $query = $this->db->prepare($sql);
        $query->execute([]);
        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function isExists($id) {
        return $this->getPage($id) !== false;
    }
}