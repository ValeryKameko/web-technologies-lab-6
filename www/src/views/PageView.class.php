<?php

namespace View;

class PageView extends View {
    public function __construct() {
        parent::__construct();
    }

    public function render($parameters) {
        $template = $this->templateEngineEnvironment->load('page.tmpl');
        $template->render($parameters);
    }
}