<?php

namespace View;

class HistoryView extends View {
    public function __construct() {
        parent::__construct();
    }

    public function render($parameters) {
        $template = $this->templateEngineEnvironment->load('history.tmpl');
        $template->render($parameters);
    }
}