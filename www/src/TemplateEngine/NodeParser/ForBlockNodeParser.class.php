<?php

namespace TemplateEngine\NodeParser;

use TemplateEngine\Parser;
use TemplateEngine\Token;
use TemplateEngine\Node\ForNode;
use TemplateEngine\Node\Expression\NameExpressionNode;
use TemplateEngine\Error\SyntaxError;

class ForBlockNodeParser extends AbstractNodeParser
{
    public function __construct()
    {

    }

    public function subparse(Parser $parser)
    {
        $line = $parser->getLine();
        $parser->expect(Token::NAME_TYPE, 'for');
        $forIterator = $parser->parseExpression();
        if (!($forIterator instanceof NameExpressionNode))
            throw new SyntaxError(
                'Iterator must be variable', 
                $parser->getLine(), 
                $parser->getSource());
        $parser->expect(Token::NAME_TYPE, 'in');
        $forExpression = $parser->parseExpression();
        $parser->expect(Token::BLOCK_END_TYPE);

        $forBody = $parser->subparse(false);

        $parser->expect(Token::NAME_TYPE, 'endfor');
        
        return new ForNode($forIterator, $forExpression, $forBody, $line);
    }
}