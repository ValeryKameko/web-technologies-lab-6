<?php

namespace TemplateEngine\NodeParser;

use TemplateEngine\Parser;
use TemplateEngine\Token;
use TemplateEngine\Node\ConfigIncludeBlockNode;

class ConfigIncludeBlockNodeParser extends AbstractNodeParser
{
    public function __construct()
    {

    }

    public function subparse(Parser $parser)
    {
        $line = $parser->getLine();
        $parser->expect(Token::NAME_TYPE, 'config_include');
        $configIncludeFilePathExpression = $parser->parseExpression();
        return new ConfigIncludeBlockNode($configIncludeFilePathExpression, $line);
    }
}