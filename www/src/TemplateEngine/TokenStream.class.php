<?php

namespace TemplateEngine;

use TemplateEngine\Source;
use TemplateEngine\Error\SyntaxError;

class TokenStream 
{
    private $current;
    private $tokens;
    private $source;

    public function __construct(array $tokens, Source $source = NULL)
    {
        $this->current = 0;
        $this->tokens = $tokens;
        $this->source = $source;
    }

    public function getSource() : Source
    {
        return $this->source;
    }

    public function next(): Token
    {
        if (!isset($this->tokens[++$this->current]))
            throw new SyntaxError(
                'Unexpected end of template', 
                $this->tokens[$this->current - 1]->getLine(), 
                $this->source);
        return $this->tokens[$this->current - 1];
    }

    public function currentToken()
    {
        return $this->tokens[$this->current];
    }

    public function test($type, $values = NULL): bool
    {
        return $this->currentToken()->test($type, $values);
    }

    public function expect($type, $values = NULL): Token
    {
        $token = $this->tokens[$this->current];
        if (!$token->test($type, $values))
            throw new SyntaxError(
                "Unexpected token \"" . $token->getValue() . 
                "\" with type " . $token->getTypeString(),
                $this->tokens[count($this->tokens) - 1]->getLine(),
                $this->source);
        $this->next();
        return $token;
    }

    public function lookup($number): Token
    {
        if (!isset($this->tokens[$this->current + $number]))
            throw new SyntaxError(
                'Unexpected end of template', 
                $this->tokens[count($this->tokens) - 1]->getLine(),
                $this->source);
        return $this->tokens[$this->current + $number];
    }

    public function isEOF() : bool
    {
        return $this->test(Token::EOF_TYPE);
    }

    public function __toString() : string
    {
        return implode(PHP_EOL, $this->tokens);
    }
}