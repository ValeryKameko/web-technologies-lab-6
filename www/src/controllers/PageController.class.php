<?php

namespace Controller;

use View\PageView;
use Model\PageModel;
use Database\SiteDatabase;
use Model\HistoryModel;

class PageController extends Controller {
    private $db;

    public function __construct() {
        parent::__construct();
        $this->db = SiteDatabase::getDatabase();
        $this->view = new PageView();
        $this->model = new PageModel($this->db->getPDO());
        $this->history = new HistoryModel();
    }

    public function handle($options) {
        $id = $options['id'];
        if (!isset($id))
            Controller::notFound();
        $page = $this->model->getPage($id);
        if (empty($page))
            Controller::notFound();

        $this->history->addEntry([
            'url' => "{$_SERVER['SERVER_NAME']}{$_SERVER['REQUEST_URI']}",
            'time' => (new \DateTime())->format('Y-m-d H:i:s')
        ]);

        $this->view->render([
            'page' => $page,
            'hasPrev' => $this->model->isExists($id - 1),
            'hasNext' => $this->model->isExists($id + 1),
        ]);
    }
}