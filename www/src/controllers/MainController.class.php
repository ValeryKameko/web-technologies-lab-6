<?php

namespace Controller;

use View\MainView;
use Model\PageModel;
use Database\SiteDatabase;
use Model\HistoryModel;

class MainController extends Controller {
    private $db;

    public function __construct() {
        parent::__construct();
        $this->db = SiteDatabase::getDatabase();
        $this->view = new MainView();
        $this->model = new PageModel($this->db->getPDO());
        $this->history = new HistoryModel();
    }

    public function handle($options) {
        $pages = $this->model->getPageIds();

        $this->history->addEntry([
            'url' => "{$_SERVER['SERVER_NAME']}{$_SERVER['REQUEST_URI']}",
            'time' => (new \DateTime())->format('Y-m-d H:i:s')
        ]);

        $this->view->render([
            'pages' => $pages
        ]);
    }
}