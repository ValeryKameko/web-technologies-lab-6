<?php

namespace Controller;

use View\HistoryView;
use Model\HistoryModel;

class HistoryController extends Controller {
    public function __construct() {
        parent::__construct();
        $this->view = new HistoryView();
        $this->model = new HistoryModel();
        $this->model->initialize();
    }

    public function handle($options) {
        $entries = $this->model->getEntries();

        $this->view->render([
            'entries' => $entries
        ]);
    }
}