<?php

namespace Controller;

use View\HistoryView;
use Model\HistoryModel;

class ClearController extends Controller {
    public function __construct() {
        parent::__construct();
        $this->model = new HistoryModel();
        $this->model->initialize();
    }

    public function handle($options) {
        $this->model->clearEntries();
        if (isset($options['url']))
            Controller::redirect($options['url']);
    }
}