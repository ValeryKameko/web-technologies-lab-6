<?php

include_once __DIR__ . '/src/Autoloader.php';

Autoloader::setBaseDirectory(__DIR__ . '/src');
Autoloader::addPrefix('TemplateEngine', 'TemplateEngine');
Autoloader::addPrefix('Model', 'models');
Autoloader::addPrefix('Controller', 'controllers');
Autoloader::addPrefix('View', 'views');
Autoloader::addPrefix('Database', 'databases');
Autoloader::addPrefix('', '');
Autoloader::register();

$router = new Router();

$router->addRoute('/^\/?$/', function(...$routeParams) {
    Controller\Controller::redirect('/history');
});

$router->addRoute('/^\/?info$/', function(...$routeParams) {
    phpinfo();
});

$router->addRoute('/^\/?main$/', function(...$routeParams) {
    $controller = new Controller\MainController();
    $controller->handle($_GET);
});

$router->addRoute('/^\/?history$/', function(...$routeParams) {
    $controller = new Controller\HistoryController();
    $controller->handle($_GET);
});

$router->addRoute('/^\/?page\?id=[^&]+$/', function(...$routeParams) {
    $controller = new Controller\PageController ();
    $controller->handle($_GET);
});

$router->addRoute('/^\/?clear\?url=[^&]+$/', function(...$routeParams) {
    $controller = new Controller\ClearController();
    $controller->handle($_GET);
});

$router->route();